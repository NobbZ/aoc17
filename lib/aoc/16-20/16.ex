defmodule AoC.Day16 do
  @moduledoc false

  use AoC.Boilerplate, transform: fn raw -> raw
    |> String.split(",")
    |> Stream.map(fn
      "s" <> str ->
        {:s, String.to_integer(str)}
      "x" <> str ->
        [p1, p2] = str |> String.split("/") |> Enum.map(&String.to_integer/1)
        {:x, p1, p2}
      "p" <> str ->
        [<<d1::utf8>>, <<d2::utf8>>] = str |> String.split("/")
        {:p, d1, d2}
    end)
    |> Enum.into([])
  end

  @dancers "abcdefghijklmnop"

  @repititions 1_000_000_000

  def a(dancers \\ @dancers, input \\ processed()) do
    do_dance(dancers, input, 1, [dancers])
  end

  def b(input \\ processed()) do
    do_dance(input, @repititions)
  end

  defp do_dance(dancers \\ @dancers, input, reps, dances \\ [@dancers])
  defp do_dance(dancers, _, 0, _), do: dancers
  defp do_dance("abcdefghijklmnop", _, _, ["abcdefghijklmnop"|dances = [_|_]]) do
    cycle = Enum.count(dances)
    pos = rem(@repititions, cycle)
    dances |> Enum.reverse() |> Enum.at(pos)
  end
  defp do_dance(dancers, input, reps, dances) do
    dancers = input
    |> Enum.reduce(dancers, fn
      {:s, pos}, acc ->
        fs = byte_size(acc) - pos
        <<f::binary-size(fs), b::binary>> = acc
        b <> f
      {:x, p1, p2}, acc ->
        swap(acc, <<:binary.at(acc, p1)::utf8>>, <<:binary.at(acc, p2)::utf8>>)
      {:p, d1, d2}, acc ->
        swap(acc, <<d1::utf8>>, <<d2::utf8>>)
    end)

    do_dance(dancers, input, reps - 1, [dancers|dances])
  end

  defp swap(str, d1, d2, acc \\ "")
  defp swap(<<d1::utf8, str::binary>>, <<d1::utf8>>, d2, acc), do: swap(str, <<d1::utf8>>, d2, acc <> d2)
  defp swap(<<d2::utf8, str::binary>>, d1, <<d2::utf8>>, acc), do: swap(str, d1, <<d2::utf8>>, acc <> d1)
  defp swap(<<c::utf8, str::binary>>, d1, d2, acc), do: swap(str, d1, d2, <<acc::binary, c::utf8>>)
  defp swap("", _, _, acc), do: acc
end
