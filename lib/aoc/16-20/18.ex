defmodule AoC.Day18 do
  @moduledoc false

  defmodule ProgB do
    defstruct pc: 0, msgs: :queue.new(), regs: %{}, cnt: 0, pid: nil, code: [], rcv: false
  end

  use AoC.Boilerplate, transform: fn raw -> raw
    |> String.split("\n")
    |> Stream.map(&String.split/1)
    |> Stream.map(fn
      [cmd, reg, val] ->
        reg = case Integer.parse(reg) do
          {n, ""} -> n
          :error -> String.to_atom(reg)
        end
        val = case Integer.parse(val) do
          {n, ""} -> n
          :error -> String.to_atom(val)
        end
        {String.to_atom(cmd), reg, val}
      [cmd, reg] ->
        {String.to_atom(cmd), String.to_atom(reg)}
    end)
    |> Enum.into([])
  end

  def a(input \\ processed()) do
    input
    |> Enum.with_index(0)
    |> Enum.into(%{}, fn {cmd, idx} -> {idx, cmd} end)
    |> compute()
  end

  def b(input \\ processed()) do
    code = input
    |> Enum.with_index(0)
    |> Enum.into(%{}, fn {cmd, idx} -> {idx, cmd} end)

    p0 = %ProgB{pid: 0, code: code, regs: %{p: 0}}
    p1 = %ProgB{pid: 1, code: code, regs: %{p: 1}}

    {_, _, r} = run_programs(p0, p1)
    r
  end

  defp run_programs(%ProgB{rcv: true, pid: 0, cnt: c0}, %ProgB{rcv: true, pid: 1, cnt: c1}), do: {:deadlock, c0, c1}
  defp run_programs(p1 = %ProgB{rcv: true, pid: 1}, p0 = %ProgB{rcv: true, pid: 0}), do: run_programs(p0, p1)
  defp run_programs(p0, p1) do
    case run_program(p0) do
      {:snd, value, p0} ->
        run_programs(p0, %{p1 | msgs: :queue.in(value, p1.msgs), rcv: false})
      {:rcv, p0} ->
        run_programs(p1, p0)
    end
  end

  defp run_program(p) do
    case p.code[p.pc] do
      {cmd, reg, val} when cmd in [:add, :mul, :mod, :set] ->
        run_program(%{p | pc: p.pc + 1, regs: reg_update(p.regs, cmd, reg, val)})
      {:jgz, reg, val} ->
        step = if reg_or_lit(p.regs, reg) > 0, do: reg_or_lit(p.regs, val), else: 1
        run_program(%{p | pc: p.pc + step})
      {:snd, val} ->
        {:snd, reg_or_lit(p.regs, val), %{p | pc: p.pc + 1, cnt: p.cnt + 1}}
      {:rcv, reg} ->
        case :queue.out(p.msgs) do
          {{:value, val}, msgs} ->
            p = %{p | pc: p.pc + 1, msgs: msgs, rcv: false, regs: Map.put(p.regs, reg, val)}
            run_program(p)
          {:empty, _} ->
            {:rcv, %{p | rcv: true}}
        end
      nil -> nil
    end
  end

  defp reg_or_lit(_regs, val) when is_number(val), do: val
  defp reg_or_lit(regs, val) when is_atom(val), do: regs[val] || 0

  defp compute(cmds, pc \\ 0, regs \\ %{}, snd \\ 0)
  defp compute(cmds, pc, regs, snd) do
    case cmds[pc] do
      {cmd, reg, val} when cmd in [:add, :mul, :mod, :set] ->
        compute(cmds, pc + 1, reg_update(regs, cmd, reg, val), snd)
      {:jgz, reg, val} ->
        val = reg_or_lit(regs, val)
        step = if reg_or_lit(regs, reg) > 0, do: val, else: 1
        compute(cmds, pc + step, regs, snd)
      {:snd, reg} ->
        compute(cmds, pc + 1, regs, reg_or_lit(regs, reg))
      {:rcv, _reg} ->
        snd
    end
  end

  defp reg_update(regs, :add, reg, val), do: reg_update(regs, &(&1 + &2), reg, val)
  defp reg_update(regs, :mul, reg, val), do: reg_update(regs, &(&1 * &2), reg, val)
  defp reg_update(regs, :mod, reg, val), do: reg_update(regs, &rem(&1, &2), reg, val) # &(rem(rem(&1, &2) + &2, &2))    (n % m + m) % m
  defp reg_update(regs, :set, reg, val), do: reg_update(regs, fn _, b -> b end, reg, val)
  defp reg_update(regs, fun, reg, val) when is_function(fun, 2) and is_atom(val), do: reg_update(regs, fun, reg, regs[val] || 0)
  defp reg_update(regs, fun, reg, val) when is_function(fun, 2) and is_integer(val), do: Map.put(regs, reg, fun.(regs[reg] || 0, val))
end
