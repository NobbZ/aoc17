defmodule AoC.Day17 do
  @moduledoc false

  use AoC.Boilerplate, raw: "344", transform: &String.to_integer/1

  def a(input \\ processed()) do
    input
    |> vortex(2017)
    |> Enum.drop_while(&(&1 != 2017))
    |> tl()
    |> hd()
  end

  def b(input \\ processed()) do
    input
    |> sim_vortex(50_000_001)
  end

  defp vortex(input, last), do: vortex(input, last + 1, 1, 0, [])

  defp vortex(_input, last, last, _pos, acc), do: acc
  defp vortex(input, last, curr, pos, acc) do
    at = rem(pos + input, curr)
    acc = insert(acc, at, curr)
    vortex(input, last, curr + 1, at + 1, acc)
  end

  defp sim_vortex(input, target), do: sim_vortex(input, 2, target, 1, 1)

  defp sim_vortex(_, curr, curr, _, val), do: val
  defp sim_vortex(input, curr, target, pos, val) do
    pos = rem(pos + input, curr ) + 1
    val = if pos == 1, do: curr, else: val
    sim_vortex(input, curr + 1, target, pos, val)
  end

  defp insert([], _, e), do: [e]
  defp insert([h|t], 0, e), do: [h, e|t]
  defp insert([h|t], at, e), do: [h|insert(t, at - 1, e)]
end
