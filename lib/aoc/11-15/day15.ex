defmodule AoC.Day15 do
  @moduledoc false

  use AoC.Boilerplate, transform: fn raw -> raw
    |> String.split("\n")
    |> Enum.map(fn <<"Generator ", _::utf8, " starts with ", number::binary>> -> number end)
    |> Enum.map(&String.to_integer/1)
    |> List.to_tuple()
  end

  use Bitwise

  @factor_a 16807
  @factor_b 48271

  @divisor_a 4
  @divisor_b 8

  @divisor 2147483647

  @samples_a 40_000_000
  @samples_b  5_000_000

  def a({sa, sb} \\ processed()), do: count(gen(sa, @factor_a), gen(sb, @factor_b), @samples_a)

  def b({sa, sb} \\ processed()), do: count(gen(sa, @factor_a, @divisor_a), gen(sb, @factor_b, @divisor_b), @samples_b)

  defp gen(n, f) do
    next = rem(n * f, @divisor)
    {next, fn -> gen(next, f) end}
  end

  defp gen(n, f, d) do
    next = rem(n * f, @divisor)
    if rem(next, d) == 0, do: {next, fn -> gen(next, f, d) end}, else: gen(next, f, d)
  end

  defp count(gena, genb, remaining, cnt \\ 0)
  defp count(_, _, 0, cnt), do: cnt
  defp count({va, ga}, {vb, gb}, remaining, cnt) do
    cnt = cnt + if (va &&& 0xffff) == (vb &&& 0xffff), do: 1, else: 0

    count(ga.(), gb.(), remaining - 1, cnt)
  end
end
