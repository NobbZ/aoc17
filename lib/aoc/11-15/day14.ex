defmodule AoC.Day14 do
  @moduledoc false

  use AoC.Boilerplate, raw: "oundnydw"

  alias AoC.Day10r, as: Day10

  def a(input \\ processed()) do
    input
    |> hash()
    |> Stream.map(&count_ones/1)
    |> Enum.sum()
  end

  def b(input \\ processed()) do
    input
    |> hash()
    |> Stream.zip(Stream.iterate(0, &(&1 + 1)))
    |> Stream.flat_map(fn {str, row} ->
      list_of_ones(str, row)
    end)
    |> Enum.into(%MapSet{})
    |> count_groups()
  end

  def hash(input) do
    0..127
    |> Stream.map(&("#{input}-#{&1}"))
    |> Stream.map(&Day10.b/1)
  end

  [{"0", 0}, {"1", 1}, {"2", 1}, {"3", 2}, {"4", 1}, {"5", 2}, {"6", 2}, {"7", 3},
   {"8", 1}, {"9", 2}, {"a", 2}, {"b", 3}, {"c", 2}, {"d", 3}, {"e", 3}, {"f", 4}]
  |> Enum.each(fn {prefix, ones} ->
    defp count_ones(unquote(prefix) <> r), do: count_ones(r) + unquote(ones)
  end)
  defp count_ones(""), do: 0

  defp list_of_ones(binary, row, col \\ 0, acc \\ [])
  defp list_of_ones("0" <> t, row, col, acc), do: list_of_ones(t, row, col + 4, acc)
  defp list_of_ones("1" <> t, row, col, acc), do: list_of_ones(t, row, col + 4, [                                            {row, col + 3}|acc])
  defp list_of_ones("2" <> t, row, col, acc), do: list_of_ones(t, row, col + 4, [                            {row, col + 2}                |acc])
  defp list_of_ones("3" <> t, row, col, acc), do: list_of_ones(t, row, col + 4, [                            {row, col + 2}, {row, col + 3}|acc])
  defp list_of_ones("4" <> t, row, col, acc), do: list_of_ones(t, row, col + 4, [            {row, col + 1}                                |acc])
  defp list_of_ones("5" <> t, row, col, acc), do: list_of_ones(t, row, col + 4, [            {row, col + 1},                 {row, col + 3}|acc])
  defp list_of_ones("6" <> t, row, col, acc), do: list_of_ones(t, row, col + 4, [            {row, col + 1}, {row, col + 2}                |acc])
  defp list_of_ones("7" <> t, row, col, acc), do: list_of_ones(t, row, col + 4, [            {row, col + 1}, {row, col + 2}, {row, col + 3}|acc])
  defp list_of_ones("8" <> t, row, col, acc), do: list_of_ones(t, row, col + 4, [{row, col}                                                |acc])
  defp list_of_ones("9" <> t, row, col, acc), do: list_of_ones(t, row, col + 4, [{row, col},                                 {row, col + 3}|acc])
  defp list_of_ones("a" <> t, row, col, acc), do: list_of_ones(t, row, col + 4, [{row, col},                 {row, col + 2}                |acc])
  defp list_of_ones("b" <> t, row, col, acc), do: list_of_ones(t, row, col + 4, [{row, col},                 {row, col + 2}, {row, col + 3}|acc])
  defp list_of_ones("c" <> t, row, col, acc), do: list_of_ones(t, row, col + 4, [{row, col}, {row, col + 1}                                |acc])
  defp list_of_ones("d" <> t, row, col, acc), do: list_of_ones(t, row, col + 4, [{row, col}, {row, col + 1},                 {row, col + 3}|acc])
  defp list_of_ones("e" <> t, row, col, acc), do: list_of_ones(t, row, col + 4, [{row, col}, {row, col + 1}, {row, col + 2}                |acc])
  defp list_of_ones("f" <> t, row, col, acc), do: list_of_ones(t, row, col + 4, [{row, col}, {row, col + 1}, {row, col + 2}, {row, col + 3}|acc])
  defp list_of_ones("", _, 128, acc), do: acc

  defp count_groups(set, count \\ 0)
  defp count_groups(set, count) when %MapSet{} == set, do: count
  defp count_groups(set, count) do
    next = Enum.at(set, 0)

    set = remove_neighbours(set, next)

    count_groups(set, count + 1)
  end

  defp remove_neighbours(set, c = {x, y}) do
    set = MapSet.delete(set, c)

    [{x + 1, y}, {x - 1, y}, {x, y + 1}, {x, y - 1}]
    |> Enum.filter(&Enum.member?(set, &1))
    |> Enum.reduce(set, &remove_neighbours(&2, &1))
  end
end
