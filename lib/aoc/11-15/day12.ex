defmodule AoC.Day12 do
  @moduledoc false

  use AoC.Boilerplate, transform: fn raw -> raw
    |> String.split("\n")
    |> Stream.map(&String.split(&1, " <-> "))
    |> Stream.map(fn [pid, pipes] ->
      pid = String.to_integer(pid)
      pipes = pipes
      |> String.split(", ")
      |> Enum.map(&String.to_integer/1)
      {pid, pipes}
    end)
    |> Enum.into(%{})
  end

  def a(input \\ processed()) do
    input
    |> walk(0)
    |> elem(0)
    |> Enum.count()
  end

  def b(input \\ processed()), do: count_groups(input)

  defp walk(map, idx), do: walk(map, idx, MapSet.new([idx]))

  defp walk(map, idx, set) do
    {pipes, map} = Map.pop(map, idx, [])

    set = Enum.into(pipes, set)

    Enum.reduce(pipes, {set, map}, fn pid, {set, map} -> walk(map, pid, set) end)
  end

  defp count_groups(map, cnt \\ 0)
  defp count_groups(map, cnt) when map == %{}, do: cnt
  defp count_groups(map, cnt) do
    {pid, _} = Enum.at(map, 0, [])

    {_, map} = walk(map, pid)

    count_groups(map, cnt + 1)
  end
end