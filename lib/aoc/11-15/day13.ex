defmodule AoC.Day13 do
  @moduledoc false

  @compile [{:native, :o3}]

  use AoC.Boilerplate, transform: fn raw -> raw
    |> String.split("\n")
    |> Stream.map(&String.split(&1, ": "))
    |> Stream.map(fn x -> Enum.map(x, &String.to_integer/1) end)
    |> Stream.map(&List.to_tuple/1)
    |> Enum.into([])
  end

  def a(input \\ processed()) do
    input
    |> prepare()
    |> severities(0)
    |> Enum.sum()
  end

  def b(input \\ processed()) do
    0
    |> Stream.iterate(&(&1 + 1))
    |> Stream.transform(prepare(input), fn n, fw -> {[severities(fw, n)], fw} end)
    |> Enum.reduce_while(0, fn sevs, acc ->
      if Enum.empty?(sevs), do: {:halt, acc}, else: {:cont, acc + 1}
    end)
  end

  defp prepare(input) do
    input
    |> Enum.sort_by(&elem(&1, 0))
    |> Enum.map(fn {idx, height} -> {idx, height, 2 * (height - 1)} end)
  end
  
  defp severities(fw, start) do
    fw
    |> Stream.filter(fn {pos, _, interval} -> rem(start + pos, interval) == 0 end)
    |> Stream.map(fn {pos, size, _} -> pos * size end)
  end
end