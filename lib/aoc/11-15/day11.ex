defmodule AoC.Day11 do
  @moduledoc false

  use AoC.Boilerplate, transform: &String.split(&1, ",")

  # More about the used coordinates at:
  # https://www.redblobgames.com/grids/hexagons/
  #
  # I have choosen to use “cube coordinates” from that linked article

  def a(dirs \\ processed()) do
    dirs
    |> Enum.reduce({0, 0, 0}, &move/2)
    |> manhattan
  end

  def b(dirs \\ processed()) do
    dirs
    |> Enum.reduce({{0, 0, 0}, 0}, fn dir, {pos, dist} ->
      pos = move(dir, pos)
      dist = max(dist, manhattan(pos))
      {pos, dist}
    end)
    |> elem(1)
  end

  defp move("n",  {x, y, z}), do: {x, y + 1, z - 1}
  defp move("ne", {x, y, z}), do: {x + 1, y, z - 1}
  defp move("se", {x, y, z}), do: {x + 1, y - 1, z}
  defp move("s",  {x, y, z}), do: {x, y - 1, z + 1}
  defp move("sw", {x, y, z}), do: {x - 1, y, z + 1}
  defp move("nw", {x, y, z}), do: {x - 1, y + 1, z}

  defp manhattan({x, y, z}), do: div(abs(x) + abs(y) + abs(z), 2)
end
