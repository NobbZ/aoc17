defmodule AoC17.NIF do
  use Rustler, otp_app: :aoc, crate: "aoc17_nif"

  def sparse(_, _), do: throw :nif_not_loaded
end
