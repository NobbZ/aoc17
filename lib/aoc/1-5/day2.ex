defmodule AoC.Day2 do
  @moduledoc false

  use AoC.Boilerplate, transform: fn raw -> raw
    |> String.split("\n")
    |> Enum.map(&String.split/1)
    |> Enum.map(fn e -> Enum.map(e, &String.to_integer/1) end)
  end
  
  @type sheet :: list(list(integer))

  @spec a(sheet) :: integer
  def a(sheet \\ processed()) do
    sheet
    |> Stream.map(&min_max/1)
    |> Stream.map(fn {l, u} -> u - l end)
    |> Enum.sum()
  end

  @spec b(sheet) :: integer
  def b(sheet \\ processed()) do
    sheet
    |> Stream.map(&Enum.sort/1)
    |> Stream.map(&Enum.reverse/1)
    |> Stream.map(&tails/1)
    |> Stream.map(fn (l) -> :lists.filtermap(&do_div/1, l) end)
    |> Stream.filter(fn (l) -> not Enum.empty?(l) end)
    |> Stream.map(&hd/1)
    |> Enum.sum()
  end

  @spec min_max(list(integer)) :: {integer, integer}
  defp min_max([]), do: {0, 0}
  defp min_max(l) do
    acc = l
    |> hd()
    |> (&({&1, &1})).()

    l
    |> Enum.reduce(acc, fn
      n, {l, u} when n < l -> {n, u}
      n, {l, u} when n > u -> {l, n}
      _, acc -> acc
    end)
  end

  @spec tails(list(a)) :: list(list(a)) when a: var
  defp tails([]), do: []
  defp tails(l = [_|t]), do: [l|tails(t)]

  @spec do_div(list(integer)) :: false | {true, integer}
  defp do_div([]), do: false
  defp do_div([_]), do: false
  defp do_div([n, m|_]) when rem(n, m) == 0, do: {true, div(n, m)}
  defp do_div([n, _|t]), do: do_div([n|t])
end
