defmodule AoC.Day4 do
  @moduledoc false

  use AoC.Boilerplate, transform: fn raw -> raw
    |> String.split("\n")
    |> Enum.map(&String.split/1)
  end

  @type phrase :: list(String.t)

  @spec a(list(phrase)) :: integer
  def a(phrases \\ processed()) do
    phrases
    |> Stream.reject(&Enum.empty?/1)
    |> Stream.map(&Enum.sort/1)
    |> Stream.filter(&Kernel.===(Enum.uniq(&1), &1))
    |> Enum.count()
  end

  @spec b(list(phrase)) :: integer
  def b(phrases \\ processed()) do
    phrases
    |> Stream.reject(&Enum.empty?/1)
    |> Stream.map(fn p -> Enum.map(p, &String.graphemes/1) end)
    |> Stream.map(fn p -> Enum.map(p, &Enum.sort/1) end)
    |> Stream.filter(&Kernel.===(Enum.uniq(&1), &1))
    |> Enum.count()
  end
end
