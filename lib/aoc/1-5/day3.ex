defmodule AoC.Day3 do
  @moduledoc false

  use AoC.Boilerplate, raw: "347991", transform: &String.to_integer/1

  @neighbours for x <- -1..1, y <- -1..1, not (x == 0 and y == 0), do: {x, y}

  @type direction :: :up | :down | :left | :right

  @spec a(integer) :: integer
  def a(cell \\ processed()) do
    coords()
    |> Enum.at(cell - 1)
    |> manhattan()
  end

  @spec b(integer) :: integer
  def b(threshold \\ processed()) do
    coords()
    |> Stream.transform(%{}, fn coord, acc ->
      sum = sum_neighbours(acc, coord)
      {[sum], Map.put(acc, coord, sum)}
    end)
    |> Stream.filter(&Kernel.>(&1, threshold))
    |> Enum.take(1)
    |> hd()
  end

  @spec sum_neighbours(map, {integer, integer}) :: integer
  defp sum_neighbours(_map, {0, 0}), do: 1
  defp sum_neighbours(map,  {x, y}) do
    @neighbours
    |> Stream.map(fn {xo, yo} -> {x + xo, y + yo} end)
    |> Stream.map(&Map.get(map, &1, 0))
    |> Enum.sum()
  end

  @spec coords() :: Enum.t
  defp coords, do: Stream.unfold({0, 0, :right}, fn acc = {x, y, _dir} -> {{x, y}, next_pos(acc)} end)

  @spec next_pos({integer, integer, direction}) :: {integer, integer, direction}
  defp next_pos({x, y, :right}) when                       x - 1  === abs(y), do: next_pos({x, y, :up})
  defp next_pos({x, y, :up})    when x >= 0 and y >= 0 and x      === y,      do: next_pos({x, y, :left})
  defp next_pos({x, y, :left})  when x < 0  and y >= 0 and abs(x) === y,      do: next_pos({x, y, :down})
  defp next_pos({x, y, :down})  when x < 0  and y < 0  and x      === y,      do: next_pos({x, y, :right})
  defp next_pos({x, y, :right}), do: {x + 1, y,     :right}
  defp next_pos({x, y, :up}),    do: {x,     y + 1, :up}
  defp next_pos({x, y, :left}),  do: {x - 1, y,     :left}
  defp next_pos({x, y, :down}),  do: {x,     y - 1, :down}

  @spec manhattan({integer, integer}) :: integer
  defp manhattan({x, y}), do: abs(x) + abs(y)
end
