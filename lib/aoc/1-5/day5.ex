defmodule AoC.Day5 do
  @moduledoc false

  use AoC.Boilerplate, transform: fn raw -> raw
    |> String.split()
    |> Enum.map(&String.to_integer/1)
  end

  @type zypper :: zypper(any)
  @type zypper(a) :: {list(a), list(a), a | :"$EOI$"}

  @spec a(list(integer)) :: non_neg_integer
  def a(instructions \\ processed()) do
    do_it(instructions, &(&1 + 1))
  end

  @spec b(list(integer)) :: non_neg_integer
  def b(instructions \\ processed()) do
    do_it(instructions, fn
      v when v >= 3 -> v - 1
      v -> v + 1
    end)
  end

  @spec do_it(list(integer), (integer -> integer)) :: non_neg_integer
  defp do_it(instructions, f) do
    instructions
    |> prepare()
    |> walk(f)
  end

  @spec prepare(list(integer)) :: zypper(integer)
  defp prepare([]), do: {[], [], :"$EOI$"}
  defp prepare([h|t]), do: {[], t, h}

  @spec walk(zypper(integer), (integer -> integer), integer) :: integer
  defp walk(instrs, f, cnt \\ 0)
  defp walk({_, _, :"$EOI$"}, _, cnt), do: cnt
  defp walk(instrs, f, cnt), do: instrs |> step(f) |> walk(f, cnt + 1)

  @spec step(zypper(integer), (integer -> integer)) :: zypper(integer)
  defp step({prev, next, curr}, f), do: do_step({prev, next, f.(curr)}, curr)

  @spec do_step(zypper(integer), integer) :: zypper(integer)
  defp do_step(instrs, 0), do: instrs
  defp do_step({[], _, _}, n) when is_integer(n) and n < 0, do: {[], [], :"$EOI$"}
  defp do_step({_, [], _}, n) when is_integer(n) and n > 0, do: {[], [], :"$EOI$"}
  defp do_step({prev, next, curr}, n) when is_integer(n) and n < 0, do: do_step({tl(prev), [curr|next], hd(prev)}, n + 1)
  defp do_step({prev, next, curr}, n) when is_integer(n) and n > 0, do: do_step({[curr|prev], tl(next), hd(next)}, n - 1)
end
