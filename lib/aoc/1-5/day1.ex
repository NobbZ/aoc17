defmodule AoC.Day1 do
  @moduledoc false

  use AoC.Boilerplate, transform: fn raw -> raw
    |> String.to_charlist()
    |> Enum.map(fn(c) -> c - ?0 end)
  end

  @type digit :: 0..9

  @spec a(list :: list(digit)) :: integer
  def a(l1 = [h|t] \\ processed()) do
    l2 = Stream.concat(t, [h])

    calc(l1, l2)
  end

  @spec b(list(digit)) :: integer
  def b(l1 \\ processed()) do
    {f, b} = Enum.split(l1, div(length(l1), 2))
    l2 = Stream.concat([b, f])

    calc(l1, l2)
  end

  @spec calc(Enum.t, Enum.t) :: integer
  defp calc(list1, list2) do
    [list1, list2]
    |> Stream.zip()
    |> Stream.filter(fn
      {x, x} -> true
      {_, _} -> false
    end)
    |> Stream.map(&elem(&1, 0))
    |> Enum.sum()
  end
end
