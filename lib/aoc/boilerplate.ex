defmodule AoC.Boilerplate do
  @moduledoc false

  defstruct [:raw, :file, :transform]

  defmacro __using__(opts), do: using(opts, __CALLER__)

  defp using(list_opts, opts \\ %__MODULE__{}, caller)
  defp using([{:raw, raw}|t], opts, caller), do: using(t, %{opts | raw: raw}, caller)
  defp using([{:file, file}|t], opts, caller), do: using(t, %{opts | file: file}, caller)
  defp using([{:transform, transform}|t], opts, caller), do: using(t, %{opts | transform: transform}, caller)
  defp using([], opts, caller) do
    file = opts.file || String.replace_suffix(caller.file, ".ex", ".in")
    raw = opts.raw || File.read!(file)
    transformer = opts.transform || quote do
      fn a -> a end
    end

    quote do
      @external_resource unquote(file)
      @__processed__ unquote(raw) |> String.trim() |> unquote(transformer).()

      def raw(), do: unquote(raw)
      def processed(), do: @__processed__
    end
  end
end
