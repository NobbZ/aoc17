defmodule AoC.Day6 do
  @moduledoc false

  use AoC.Boilerplate, transform: fn raw -> raw
    |> String.split()
    |> Enum.map(&String.to_integer/1)
  end

  @spec a(list(integer)) :: integer
  def a(memory \\ processed()) do
    reallocate(memory, MapSet.new, fn mem, set ->
      if MapSet.member?(set, mem) do
        {:halt, Enum.count(set) + 1} # We need to count this iteration as well
      else
        {:cont, MapSet.put(set, mem)}
      end
    end)
  end

  @spec b(list(integer)) :: integer
  def b(memory \\ processed()) do
    reallocate(memory, {%{}, 0}, fn mem, {seen, iteration} ->
      case seen do
        %{^mem => last} -> {:halt, iteration - last}
        _ -> {:cont, {Map.put(seen, mem, iteration), iteration + 1}}
      end
    end)
  end

  @spec reallocate(list(integer), acc, reducer) :: acc when acc: var, reducer: fun
  defp reallocate(memory, start_acc, reducer) do
    memory
    |> to_map()
    |> to_stream()
    |> Enum.reduce_while(start_acc, reducer)
  end

  @spec to_map(list(integer)) :: %{required(integer) => integer}
  def to_map(memory) do
    memory
    |> Stream.with_index()
    |> Stream.map(fn {blocks, idx} -> {idx, blocks} end)
    |> Enum.into(%{})
  end

  @spec to_stream(map) :: Enum.t
  def to_stream(map) do
    banks = Enum.count(map)

    Stream.unfold(map, fn acc ->
      {max_idx, value} = Enum.max_by(acc, fn {_, value} -> value end)
      base = div(value, banks)
      excess = rem(value, banks)

      acc = acc
      |> Map.put(max_idx, 0)
      |> insert_base(base)
      |> insert_excess(excess, max_idx, banks)

      {acc, acc}
    end)
  end

  def insert_base(mem, 0), do: mem
  def insert_base(mem, base) do
    mem
    |> Enum.map(fn {i, v} -> {i, v + base} end)
    |> Enum.into(%{})
  end

  def insert_excess(mem, 0, _, _), do: mem
  def insert_excess(mem, excess, max_idx, size) do
    mem
    |> Map.update!(rem(max_idx + excess, size), &(&1 + 1))
    |> insert_excess(excess - 1, max_idx, size)
  end
end
