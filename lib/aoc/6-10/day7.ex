defmodule AoC.Day7 do
  @moduledoc false

  alias AoC.Day7.Reader

  use AoC.Boilerplate, transform: &Reader.read/1

  def a({n, _, _} \\ processed()), do: n

  def b(pyramid \\ processed()) do
    pyramid
    |> sum_weights()
    |> chop_tree()
    |> find_weight()
  end

  defp sum_weights({n, w, []}), do: {n, w, w, []}
  defp sum_weights({n, w, cs}) do
    cs = Enum.map(cs, &sum_weights/1)

    cw = Enum.reduce(cs, w, fn {_, _, cw, _}, sum -> sum + cw end)

    {n, w, cw, cs}
  end

  defp chop_tree({n, w, cw, c}) do
    histo = Enum.reduce(c, %{}, fn {_, _, cw, _}, acc -> Map.update(acc, cw, 1, &(1 + &1)) end)

    if Enum.count(histo) <= 1 do
      {n, w, cw, histo |> Enum.into([]) |> hd() |> elem(0)}
    else
      [hw, ow] = histo
      |> Enum.sort_by(fn {_, c} -> c end)
      |> Enum.map(&elem(&1, 0))

      c = c
      |> Enum.map(&chop_tree/1)
      |> Enum.filter(fn {_, _, cw, _} -> cw == hw end)
      |> hd()

      {n, w, cw, {c, ow}}
    end
  end

  defp find_weight({_, _, _, {{_, w, cw, n}, ow}}) when is_integer(n), do: w - (abs(cw - ow))
  defp find_weight({_, _, _, {c, _}}), do: find_weight(c)
end
