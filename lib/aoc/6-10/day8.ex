defmodule AoC.Day8 do
  @moduledoc false

  use AoC.Boilerplate, transform: fn raw -> raw
    |> String.split("\n")
    |> Stream.map(&String.split/1)
    |> Stream.reject(&Enum.empty?/1)
    |> Stream.map(fn [reg1, op1, amnt1, "if", reg2, op2, amnt2] ->
      {reg1, op1, amnt1, reg2, op2, amnt2}
    end)
    |> Stream.map(fn {r1, o1, a1, r2, o2, a2} ->
      {String.to_atom(r1), String.to_atom(o1), String.to_integer(a1),
       String.to_atom(r2), String.to_atom(o2), String.to_integer(a2)}
    end)
    |> Enum.into([])
  end

  def a(instructions \\ processed()) do
    instructions
    |> Enum.reduce({%{}, nil}, &reducer(&1, &2, fn
      _, _ -> nil
    end))
    |> elem(0)
    |> Enum.max_by(fn {_, v} -> v end)
    |> elem(1)
  end

  def b(instructions \\ processed()) do
    instructions
    |> Enum.reduce({%{}, 0}, &reducer(&1, &2, fn {_, v}, max ->
      if v > max, do: v, else: max
    end))
    |> elem(1)
  end

  def reducer({r1, o1, a1, r2, o2, a2}, {regs, result}, f) do
    p = &apply(Kernel, o2, [&1, a2])

    init = case o1 do
      :inc ->  a1
      :dec -> -a1
    end

    regs = case p.(Map.get(regs, r2, 0)) do
      true  -> Map.update(regs, r1, init, &(&1 + init))
      false -> regs
    end

    {regs, f.({r1, Map.get(regs, r1, 0)}, result)}
  end
end
