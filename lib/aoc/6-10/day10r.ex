defmodule AoC.Day10r do
  @moduledoc false

  use AoC.Boilerplate, raw: "70,66,255,2,48,0,54,48,80,141,244,254,160,108,1,41",
  transform: fn raw -> raw
    |> String.split(",")
    |> Stream.map(&String.to_integer/1)
    |> Enum.into([])
  end

  require Bitwise

  alias AoC17.NIF

  @range 0..255

  @finisher <<17, 31, 73, 47, 23>>

  def a(list \\ @range, steps \\ processed()) do
    list
    |> Enum.into([])
    |> NIF.sparse(steps)
    |> Enum.take(2)
    |> Enum.reduce(&Kernel.*/2)
  end

  def b(lengths \\ raw()) do
    steps = fn -> lengths <> @finisher end
    |> Stream.repeatedly()
    |> Enum.take(64)
    |> Enum.reduce("", fn lengths, acc -> acc <> lengths end)
    |> to_charlist()

    @range
    |> Enum.into([])
    |> NIF.sparse(steps)
    |> Enum.chunk_every(16)
    |> Enum.map(fn l -> Enum.reduce(l, &Bitwise.bxor/2) end)
    |> Enum.map(&Integer.to_string(&1, 16))
    |> Enum.map(&String.pad_leading(&1, 2, ["0"]))
    |> Enum.map(&String.downcase/1)
    |> Enum.join()
  end
end
