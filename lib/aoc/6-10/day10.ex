defmodule AoC.Day10 do
  require Bitwise

  @range 0..255

  @finish_sequence <<17, 31, 73, 47, 23>>

  use AoC.Boilerplate, raw: "70,66,255,2,48,0,54,48,80,141,244,254,160,108,1,41",
    transform: fn raw -> raw
      |> String.split(",")
      |> Stream.map(&String.to_integer/1)
      |> Enum.into([])
    end

  def a(list \\ 0..255, steps \\ processed()) do
    list
    |> calc_sparse(steps)
    |> (fn %{0 => x, 1 => y} -> x * y end).()
  end

  def b(lengths \\ raw()) do
    steps = fn -> lengths <> @finish_sequence end
    |> Stream.repeatedly()
    |> Enum.take(64)
    |> Enum.reduce("", fn lengths, acc -> acc <> lengths end)
    |> to_charlist()

    @range
    |> calc_sparse(steps)
    |> Enum.into([])
    |> Enum.sort_by(fn {i, _} -> i end)
    |> Enum.map(fn {_, v} -> v end)
    |> Enum.chunk_every(16)
    |> Enum.map(fn l -> Enum.reduce(l, &Bitwise.bxor/2) end)
    |> Enum.map(&Integer.to_string(&1, 16))
    |> Enum.map(&String.pad_leading(&1, 2, ["0"]))
    |> Enum.map(&String.downcase/1)
    |> Enum.join()
  end

  defp calc_sparse(list, steps) do
    size = Enum.count(list)

    list = list
    |> with_index()
    |> Enum.into(%{})

    steps
    |> Enum.reduce({list, 0, 0}, fn length, {list, pos, skip} ->
      list = list
      |> extract(length, pos, size)
      |> Enum.reduce({pos, []}, fn v, {pos, acc} ->
        {rem(pos + 1, size), [{pos, v}|acc]}
      end)
      |> elem(1)
      |> Enum.into(list)

      {list, rem(pos + length + skip, size), skip + 1}
    end)
    |> elem(0)
  end

  defp with_index(enum, offset \\ 0) do
    enum
    |> Stream.with_index(offset)
    |> Stream.map(fn {e, i} -> {i, e} end)
  end

  def extract(list,  length, pos,  size,  acc \\ [])
  def extract(_list, 0,      _pos, _size, acc), do: acc
  def extract(list,  length, pos,  size,  acc), do:
    extract(list, length - 1, rem(pos + 1, size), size, [list[pos]|acc])
end
