defmodule AoC.Day7.Reader do
  @moduledoc false

  def read(input) do
    data = input
    |> String.split("\n")
    |> Stream.map(&String.split/1)
    |> Stream.map(fn
      [name, weight] ->
        {name, weight, []}
      [name, weight, "->"|children] ->
        {name, weight, children}
    end)
    |> Stream.map(fn {n, w, cs} -> {n, w |> String.slice(1..-2), cs |> Enum.map(&String.trim(&1, ","))} end)
    |> Stream.map(fn {n, w, cs} -> {n, String.to_integer(w), cs} end)
    |> Enum.into(%{}, fn {n, w, cs} -> {n, {w, cs}} end)

    parents = Enum.reduce(data, %{}, fn
      {n, {_, cs}}, acc ->
        Enum.reduce(cs, acc, fn
          c, acc ->
            Map.put(acc, c, n)
        end)
    end)

    root = find_root(parents)

    grow_tree(data, root)
  end

  def find_root(parents) do
    parents
    |> Enum.at(0)
    |> elem(1)
    |> find_root(parents)
  end

  def find_root(key, parents) do
    case parents[key] do
      nil  -> key
      next -> find_root(next, parents)
    end
  end

  defp grow_tree(map, root) do
    case map[root] do
      {w, c} ->
        {root, w, Enum.map(c, &grow_tree(map, &1))}
    end
  end
end
