defmodule AoC.Day9 do
  @moduledoc false

  use AoC.Boilerplate, transform: &String.codepoints/1

  def a(stream \\ processed()), do: score_groups(stream)

  def b(stream \\ processed()), do: count_garbage(stream)

  defp score_groups(stream, state \\ [], curr \\ 0, sum \\ 0)
  defp score_groups([],         [],               _,    sum), do: sum
  defp score_groups(["{"|t],    [],               0,    sum), do: score_groups(t, [:group],     1,        sum + 1)
  defp score_groups(["<"|t],    [],               0,    sum), do: score_groups(t, [:garbage],   0,        sum)
  defp score_groups(["{"|t],    s = [:group|_],   curr, sum), do: score_groups(t, [:group|s],   curr + 1, sum + curr + 1)
  defp score_groups(["}"|t],    [:group|s],       curr, sum), do: score_groups(t, s,            curr - 1, sum)
  defp score_groups(["<"|t],    s = [:group|_],   curr, sum), do: score_groups(t, [:garbage|s], curr,     sum)
  defp score_groups([","|t],    s = [:group|_],   curr, sum), do: score_groups(t, s,            curr,     sum)
  defp score_groups(["!", _|t], s = [:garbage|_], curr, sum), do: score_groups(t, s,            curr,     sum)
  defp score_groups([">"|t],    [:garbage|s],     curr, sum), do: score_groups(t, s,            curr,     sum)
  defp score_groups([_|t],      s = [:garbage|_], curr, sum), do: score_groups(t, s,            curr,     sum)

  defp count_garbage(stream, garbage \\ false, chars \\ 0)
  defp count_garbage([],         false, chars), do: chars
  defp count_garbage(["<"|t],    false, chars), do: count_garbage(t, true,  chars)
  defp count_garbage([_|t],      false, chars), do: count_garbage(t, false, chars)
  defp count_garbage(["!", _|t], true,  chars), do: count_garbage(t, true,  chars)
  defp count_garbage([">"|t],    true,  chars), do: count_garbage(t, false, chars)
  defp count_garbage([_|t],      true,  chars), do: count_garbage(t, true,  chars + 1)
end
