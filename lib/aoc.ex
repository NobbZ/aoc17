defmodule AoC do
  @moduledoc false

  @today Date.utc_today() |> Date.diff(~D[2017-11-30])

  @all 1..(min(@today, 25)) |> Stream.map(&("#{&1}")) |> Stream.flat_map(&([{&1, "a"}, {&1, "b"}])) |> Enum.into([])

  def main([]), do: Enum.each(@all, fn {n, p} -> dispatch(n, p) end)
  def main(["raw", day]), do: IO.puts Module.concat(__MODULE__, "Day#{day}").raw()
  def main(["parsed", day]), do: IO.puts inspect(Module.concat(__MODULE__, "Day#{day}").processed(), limit: :infinity, pretty: true)
  def main([num]), do: dispatch(num, "a")
  def main([num, part]), do: dispatch(num, part)

  defp dispatch(num, "a"), do: dispatch(num, :a)
  defp dispatch(num, "b"), do: dispatch(num, :b)
  defp dispatch(num, part) when {num, part} in [{"10", :b}], do:
    dispatch(num, part, ~c"~s after ~wµs~n")
  defp dispatch(num, part), do:
    dispatch(num, part, ~c"~8..·s after ~8..·wµs~n")

  @all
  |> Enum.each(fn {day, part} ->
    part = String.to_existing_atom(part)
    module = Module.concat(__MODULE__, "Day#{day}")
    defp dispatch(unquote(day), unquote(part), format) do
      :io.format(~c"~2s~1s -> ", [unquote(day), unquote(part)])
      {time, r} = :timer.tc(&unquote(module).unquote(part)/0)
      :io.format(format, [to_charlist(r), time])
    end
  end)
end
