defmodule AoCTest do
  use ExUnit.Case
  doctest AoC

  @values %{
     1 => [a:     1_047, b:        982],
     2 => [a:    46_402, b:        265],
     3 => [a:       480, b:    349_975],
     4 => [a:       386, b:        208],
     5 => [a:   374_269, b: 27_720_699],
     6 => [a:     4_074, b:      2_793],
     7 => [a: "qibuqqg", b:      1_079],
     8 => [a:     3_089, b:      5_391],
     9 => [a:    11_347, b:      5_404],
    10 => [a:     7_888, b: "decdf7d377879877173b7f2fb131cf1b"],
    "10r" => [a:     7_888, b: "decdf7d377879877173b7f2fb131cf1b"],
    11 => [a:       773, b:      1_560],
    12 => [a:       378, b:        204],
    13 => [a:     1_928, b:  3_830_344],
    14 => [a:     8_106, b:      1_164],
    15 => [a:       650, b:        336],
    16 => [a: "olgejankfhbmpidc", b: "gfabehpdojkcimnl"],
    17 => [a:       996, b:  1_898_341],
    18 => [a:     3_188, b:      7_112],
  }

  @examples %{
     1 => [{:a, [[1, 1, 2, 2]],              3},
           {:a, [[1, 1, 1, 1]],              4},
           {:a, [[1, 2, 3, 4]],              0},
           {:a, [[9, 1, 2, 1, 2, 1, 2, 9]],  9},
           {:b, [[1, 2, 1, 2]],              6},
           {:b, [[1, 2, 2, 1]],              0},
           {:b, [[1, 2, 3, 4, 2, 5]],        4},
           {:b, [[1, 2, 3, 1, 2, 3]],       12},
           {:b, [[1, 2, 1, 3, 1, 4, 1, 5]],  4}],

     2 => [{:a, [[[5, 1, 9, 5]]],  8},
           {:a, [[[7, 5, 3]]],     4},
           {:a, [[[2, 4, 6, 8]]],  6},
           {:a, [[[5, 1, 9, 5],
                 [7, 5, 3],
                 [2, 4, 6, 8]]],  18},
           {:b, [[[5, 9, 2, 8]]],  4},
           {:b, [[[9, 4, 7, 3]]],  3},
           {:b, [[[3, 8, 6, 5]]],  2},
           {:b, [[[5, 9, 2, 8],
                  [9, 4, 7, 3],
                  [3, 8, 6, 5]]],  9}],

     3 => [{:a,    [1],  0},
           {:a,   [12],  3},
           {:a,   [23],  2},
           {:a, [1024], 31},
           {:b,    [1],  2},
           {:b,    [2],  4},
           {:b,    [3],  4},
           {:b,    [4],  5},
           {:b,    [5], 10}],

     4 => [{:a, [[["aa", "bb", "cc", "dd", "ee"]]],           1},
           {:a, [[["aa", "bb", "cc", "dd", "aa"]]],           0},
           {:a, [[["aa", "bb", "cc", "dd", "aaa"]]],          1},
           {:a, [[["aa", "bb", "cc", "dd", "ee"],
                  ["aa", "bb", "cc", "dd", "aa"],
                  ["aa", "bb", "cc", "dd", "aaa"]]],          2},
           {:b, [[["abcde", "fghij"]]],                       1},
           {:b, [[["abcde", "xyz", "ecdab"]]],                0},
           {:b, [[["a", "ab", "abc", "abd", "abf", "abj"]]],  1},
           {:b, [[["iiii", "oiii", "ooii", "oooi", "oooo"]]], 1},
           {:b, [[["oiii", "ioii", "iioi", "iiio"]]],         0},
           {:b, [[["abcde", "fghij"],
                  ["abcde", "xyz", "ecdab"],
                  ["a", "ab", "abc", "abd", "abf", "abj"],
                  ["iiii", "oiii", "ooii", "oooi", "oooo"],
                  ["oiii", "ioii", "iioi", "iiio"]]],         3}],

     5 => [{:a, [[0, 3, 0, 1, -3]], 5},
           {:b, [[0, 3, 0, 1, -3]], 10}],

     6 => [{:a, [[0, 2, 7, 0]], 5},
           {:b, [[0, 2, 7, 0]], 4}],

     7 => [{:a, [{"tknk", 41, [
                   {"ugml", 68, [
                     {"gyxo", 61, []},
                     {"ebii", 61, []},
                     {"jptl", 61, []}]},
                   {"padx", 45, [
                     {"pbga", 66, []},
                     {"havc", 66, []},
                     {"qoyq", 66, []}]},
                   {"fwft", 72, [
                     {"ktlj", 57, []},
                     {"cntj", 57, []},
                     {"xhth", 57, []}]}]}], "tknk"},
           {:b, [{"tknk", 41, [
                   {"ugml", 68, [
                     {"gyxo", 61, []},
                     {"ebii", 61, []},
                     {"jptl", 61, []}]},
                   {"padx", 45, [
                     {"pbga", 66, []},
                     {"havc", 66, []},
                     {"qoyq", 66, []}]},
                   {"fwft", 72, [
                     {"ktlj", 57, []},
                     {"cntj", 57, []},
                     {"xhth", 57, []}]}]}], 60}],

     8 => [{:a, [[{:b, :inc,   5, :a, :>,    1},
                  {:a, :inc,   1, :b, :<,    5},
                  {:c, :dec, -10, :a, :>=,   1},
                  {:c, :inc, -20, :c, :==,  10}]],  1},
           {:b, [[{:b, :inc,   5, :a, :>,    1},
                  {:a, :inc,   1, :b, :<,    5},
                  {:c, :dec, -10, :a, :>=,   1},
                  {:c, :inc, -20, :c, :==,  10}]], 10}],

     9 => [{:a, [String.codepoints("{}")],                            1},
           {:a, [String.codepoints("{{{}}}")],                        6},
           {:a, [String.codepoints("{{},{}}")],                       5},
           {:a, [String.codepoints("{{{},{},{{}}}}")],               16},
           {:a, [String.codepoints("{<a>,<a>,<a>,<a>}")],             1},
           {:a, [String.codepoints("{{<ab>},{<ab>},{<ab>},{<ab>}}")], 9},
           {:a, [String.codepoints("{{<!!>},{<!!>},{<!!>},{<!!>}}")], 9},
           {:a, [String.codepoints("{{<a!>},{<a!>},{<a!>},{<ab>}}")], 3},
           {:b, [String.codepoints("<>")],                            0},
           {:b, [String.codepoints("<random characters>")],          17},
           {:b, [String.codepoints("<<<<>")],                         3},
           {:b, [String.codepoints("<{!>}>")],                        2},
           {:b, [String.codepoints("<!!>")],                          0},
           {:b, [String.codepoints("<!!!>>")],                        0},
           {:b, [String.codepoints("<{o\"i!a,<{i<a>")],              10}],

    10 => [{:a, [(0..4), [3, 4, 1, 5]], 12},
           {:b, [""],         "a2582a3a0e66e6e86e3812dcb672a272"},
           {:b, ["AoC 2017"], "33efeb34ea91902bb2f59c9920caa6cd"},
           {:b, ["1,2,3"],    "3efbe78a8d82f29979031a4aa0b16a9d"},
           {:b, ["1,2,4"],    "63960835bcdc130f0b66d7ff4f6a5a8e"}],

    "10r" => [{:a, [(0..4), [3, 4, 1, 5]], 12},
           {:b, [""],         "a2582a3a0e66e6e86e3812dcb672a272"},
           {:b, ["AoC 2017"], "33efeb34ea91902bb2f59c9920caa6cd"},
           {:b, ["1,2,3"],    "3efbe78a8d82f29979031a4aa0b16a9d"},
           {:b, ["1,2,4"],    "63960835bcdc130f0b66d7ff4f6a5a8e"}],

    11 => [{:a, [["ne", "ne", "ne"]],             3},
           {:a, [["ne", "ne", "sw", "sw"]],       0},
           {:a, [["ne", "ne", "s", "s"]],         2},
           {:a, [["se", "sw", "se", "sw", "sw"]], 3},
           {:b, [["ne", "ne", "ne"]],             3},
           {:b, [["ne", "ne", "sw", "sw"]],       2},
           {:b, [["ne", "ne", "s", "s"]],         2},
           {:b, [["se", "sw", "se", "sw", "sw"]], 3}],

    12 => [{:a, [%{0 => [2],
                   1 => [1],
                   2 => [0, 3, 4],
                   3 => [2, 4],
                   4 => [2, 3, 6],
                   5 => [6],
                   6 => [4, 5]}], 6},
           {:b, [%{0 => [2],
                   1 => [1],
                   2 => [0, 3, 4],
                   3 => [2, 4],
                   4 => [2, 3, 6],
                   5 => [6],
                   6 => [4, 5]}], 2}],

    13 => [{:a, [[{0, 3}, {1, 2}, {4, 4}, {6, 4}]], 24},
           {:b, [[{0, 3}, {1, 2}, {4, 4}, {6, 4}]], 10}],

    14 => [{:a, ["flqrgnkx"], 8108},
           {:b, ["flqrgnkx"], 1242}],

    15 => [{:a, [{65, 8921}], 588},
           {:b, [{65, 8921}], 309}],

    16 => [{:a, ["abcde", [{:s, 1}, {:x, 3, 4}, {:p, ?e, ?b}]], "baedc"}],

    17 => [{:a, [3], 638}],

    18 => [{:a, [[{:set, :a, 1},
                  {:add, :a, 2},
                  {:mul, :a, :a},
                  {:mod, :a, 5},
                  {:snd, :a},
                  {:set, :a, 0},
                  {:rcv, :a},
                  {:jgz, :a, -1},
                  {:set, :a, 1},
                  {:jgz, :a, -2}]], 4},
           {:b, [[{:snd, 1},
                  {:snd, 2},
                  {:snd, :p},
                  {:rcv, :a},
                  {:rcv, :b},
                  {:rcv, :c},
                  {:rcv, :d}]], 3}],
          #  {:b, [[{:snd, 1},
          #         {:snd, 2},
          #         {:snd, :p},
          #         {:rcv, :a},
          #         {:rcv, :b}]], 3}],
  }

  @examples
  |> Map.merge(@values, fn _, ex, v -> ex ++ v end)
  |> Enum.each(fn {day, tests} ->
    mod = Module.concat(AoC, "Day#{day}")
    testmod = Module.concat(AoCTest, "Day#{day}Test")

    defmodule testmod do
      use ExUnit.Case, async: true
      Enum.each(tests, fn
        {part, input, expect} ->
          @tag [example: true, day: day, part: part, daypart: "#{day}#{part}"]
          test "Day #{day}#{part}: #{inspect input} is #{expect}", do:
            assert apply(unquote(mod), unquote(part), unquote(Macro.escape(input))) == unquote(expect)
        {part, expect} ->
          @tag [full: true, day: day, part: part, daypart: "#{day}#{part}"] #, timeout: 5 * 60 * 1000]
          test "Day #{day}#{part}: **FULL**", do:
            assert unquote(mod).unquote(part)() == unquote(expect)
      end)
    end
  end)
end
