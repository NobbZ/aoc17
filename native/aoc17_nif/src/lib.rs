#[macro_use] extern crate rustler;
// #[macro_use] extern crate rustler_codegen;
// #[macro_use] extern crate lazy_static;

use rustler::{NifEnv, NifTerm, NifResult, NifEncoder}; //, NifError};
// use rustler::types::atom::NifAtom;

// mod atoms {
//     rustler_atoms! {
//         // atom ok;
//         //atom error;
//         //atom __true__ = "true";
//         //atom __false__ = "false";
//     }
// }

rustler_export_nifs! {
    "Elixir.AoC17.NIF",
    [("sparse", 2, sparse)],
    None
}

fn sparse<'a>(env: NifEnv<'a>, args: &[NifTerm<'a>]) -> NifResult<NifTerm<'a>> {
    let mut list: Vec<u8> = try!(args[0].decode());
    let steps: Vec<u8> = try!(args[1].decode());

    let mut pos = 0u8;
    let mut skip = 0u8;

    for s in steps {
        reverse(&mut list, pos, s);
        pos += s + skip;
        skip += 1;
    }

    Ok(list.encode(env))
}

fn reverse(list: &mut Vec<u8>, pos: u8, len: u8) {
    let i = pos as usize;
    let j = i + len as usize - 1;
    let mut k = 0;

    let size = list.len();

    while i + k < j - k {
        let n = (i + k) % size;
        let m = (j - k) % size;
        let tmp = list[n];
        list[n] = list[m];
        list[m] = tmp;
        k += 1
    }
}
